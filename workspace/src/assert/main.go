package main

import (
	"fmt"
	"image"
	"math"
	"strconv"
)

type MyInt int

func (mi MyInt) String() string {
	return fmt.Sprintf("[%d]", int(mi))
}

func printme(x any) {

	/*
		if i, ok := x.(int); ok {
			fmt.Println("int: " + strconv.Itoa(i))
		} else if s, ok := x.(string); ok {
			fmt.Println("string: " + s)
		}
	*/

	switch v := x.(type) {
	case int:
		fmt.Println("int: " + strconv.Itoa(v))
	case string:
		fmt.Println("string: " + v)
	case float64:
		fmt.Println("float64: " + strconv.FormatFloat(v, 'f', 3, 64))
	case fmt.Stringer:
		fmt.Println("Stringer: " + v.String())
	}
}

func subImage(img image.Image, r image.Rectangle) (image.Image, bool) {

	if src, ok := img.(interface {
		SubImage(r image.Rectangle) image.Image
	}); ok {
		return src.SubImage(r), true
	}
	return nil, false
}

func main() {

	printme(1)
	printme("Hello")
	printme(math.Pi)
	printme(MyInt(42))
}

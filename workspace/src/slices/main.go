package main

import "fmt"

func modify(slice []int) {
	slice[0] = 42
}

func main() {

	var slices []int // ungebunde slices

	var arr [10]int
	var arr2 [20]int

	arr[3] = 42

	slices = arr[1:len(arr)] // gebundene slice
	fmt.Println(slices)

	slices = arr[:]
	fmt.Println(slices)

	slices = slices[:3]
	fmt.Println(slices)
	fmt.Println("len:", len(slices))
	fmt.Println("len:", cap(slices))

	modify(slices)
	modify(arr2[:])

	fmt.Println(arr2[0])

	allocated := make([]int, 5, 7)
	fmt.Println(allocated, len(allocated), cap(allocated))

	//allocated = allocated[:101]

	allocated = append(allocated, 10, 20, 40, 50, 70, 89)

	fmt.Println(allocated)

}

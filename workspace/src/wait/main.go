package main

import (
	"fmt"
	"runtime"
	"sync"
)

type entry struct {
	result int
	who    int
}

func calculate(
	wg *sync.WaitGroup,
	who int,
	jobs <-chan int,
	entries []entry,
) {
	defer wg.Done()

	for job := range jobs {
		entries[job] = entry{
			result: job * job,
			who:    who,
		}
	}
}

func main() {
	entries := make([]entry, 1_000)
	cpus := runtime.NumCPU()
	jobs := make(chan int)

	var wg sync.WaitGroup

	for i := 0; i < cpus; i++ {
		wg.Add(1)
		go calculate(&wg, i, jobs, entries)

		/*
			go func(who int) {
				defer wg.Done()

				for job := range jobs {
					entries[job] = entry{
						result: job * job,
						who:    who,
					}
				}
			}(i)
		*/
	}

	for i := range entries {
		jobs <- i
	}
	close(jobs)

	wg.Wait()

	for i, entry := range entries {
		fmt.Printf("%d: %d (%d)\n", i, entry.result, entry.who)
	}
}

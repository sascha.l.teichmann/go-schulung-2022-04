package main

import (
	"fmt"
	"log"
	"regexp"
)

var lineMatcher = regexp.MustCompile("([abc]+)123")

func first() (err error) {

	defer func() {
		if x := recover(); x != nil {
			err = fmt.Errorf("panic gefangen: %v", x)
		} else {
			fmt.Println("hello from defer 1", x)
		}
	}()

	second()

	return nil
}

func second() {
	third()
}

func third() {
	panic("PANIC")
	fmt.Println("third")
}

func main() {
	if err := first(); err != nil {
		log.Printf("error: %v\n", err)
	}
}

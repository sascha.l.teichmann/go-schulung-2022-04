package main

import "fmt"

type Bremsbar interface {
	quietsch()
}

type Auto interface {
	Bremsbar
	brumm()
}

type Motor float64

func (m Motor) brumm() {
	fmt.Println("Motor: brumm")
}

type Bremse int32

func (b Bremse) quietsch() {
	fmt.Println("Bremse: quietsch")
}

func fahre(a Auto) {
	a.brumm()
	a.quietsch()
}

type Kufe int

func (k Kufe) quietsch() {
	fmt.Println("kufe: kratz!")
}

type Wagen struct {
	Motor
	Bremsbar
}

func main() {
	var w Wagen
	var bremse Bremse
	w.Bremsbar = bremse
	fahre(w)
	var kufe Kufe
	w.Bremsbar = kufe
	fahre(w)
}

package main

import "fmt"

type Base int
type SecondBase int

type Derived struct {
	Base
	SecondBase
}

func (b Base) hello() {
	fmt.Println("hello")
}

func (sb SecondBase) hello() {
	fmt.Println("second: hello")
}

func (d Derived) hello() {
	fmt.Println("derived: hello")
	d.Base.hello()
	d.SecondBase.hello()
}

func main() {
	var b Base
	b.hello()
	var d Derived

	d.hello()
}

package main

import "fmt"

func modify(f *float64) {
	*f = 3.1415
}

func grow(slice *[]int) {
	*slice = append(*slice, 1, 2, 3, 5)
}

func good() *int {
	var x int
	return &x
}

func main() {

	var f float64
	modify(&f)
	fmt.Println(f)

	*good() = 42

	var slice []int
	grow(&slice)
	fmt.Printf("%v\n", slice)
}

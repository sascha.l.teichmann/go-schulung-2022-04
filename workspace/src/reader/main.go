package main

import (
	"flag"
	"io"
	"log"
	"os"
)

type replacer struct {
	io.Reader
}

func (r *replacer) Read(p []byte) (int, error) {
	n, err := r.Reader.Read(p)

	for i, v := range p[:n] {
		switch v {
		case 'a', 'b', 'c', 'f':
			p[i] = 'A' + v - 'a'
		}
	}

	return n, err
}

func readerFactory(do bool, parent io.Reader) io.Reader {
	if do {
		return &replacer{Reader: parent}
	}
	return parent
}

func main() {
	do := flag.Bool("replace", false, "replace some runes")
	flag.Parse()

	input := readerFactory(*do, os.Stdin)

	if _, err := io.Copy(os.Stdout, input); err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

package main

import (
	"fmt"
	"log"
	"net/http"
)

type controller struct {
	books []string
}

func (c *controller) listBooks(
	resp http.ResponseWriter, req *http.Request) {

	log.Println("/books")
	for _, book := range c.books {
		fmt.Fprintf(resp, "%s\n", book)
	}
}

func main() {
	ctrl := controller{
		books: []string{
			"Dr Schiwago",
			"Herr der Ringe",
		},
	}
	mux := http.NewServeMux()
	mux.HandleFunc("/books", ctrl.listBooks)
	/*
		fn := func(resp http.ResponseWriter, req *http.Request) {
			ctrl.listBooks(resp, req)
		}
		mux.HandleFunc("/books", fn)
	*/

	log.Fatalln(http.ListenAndServe("localhost:8080", mux))
}

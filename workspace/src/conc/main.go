package main

import (
	"fmt"
)

func main() {

	//var ch chan bool
	done := make(chan struct{})

	msgs := make(chan string)

	go func() {
		// defer func() { done <- struct{}{} }()
		defer close(done)

		for txt := range msgs {
			fmt.Println(txt)
		}

		//txt := <-msgs
		//fmt.Println(txt)
	}()

	msgs <- "Hello aus der Go-Routine"
	msgs <- "Hello aus der Go-Routine 2"

	close(msgs)

	fmt.Println("Hello")
	fmt.Println("Hello")

	_, ok := <-done
	fmt.Println(ok)

	//time.Sleep(time.Nanosecond)
}

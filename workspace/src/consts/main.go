package main

import "fmt"

const (
	a = iota * 2
	b
	c
)

func main() {

	fmt.Println(a, b, c)

	const ten = 10

	//x := ten

	//var mil int = 1e6

	var y int = 0.42 * 100

	var z float64 = 3.1415

	//z = ten

	//_ = x
	_ = z
	_ = y
}

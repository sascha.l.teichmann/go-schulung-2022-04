package main

import (
	"fmt"
	"unicode/utf8"
)

func main() {
	var s string

	s = "H💩llo"

	fmt.Printf("%c\n", s[1])

	t := s[:len(s)-1]
	fmt.Println(t)

	for i, c := range s {
		fmt.Printf("%d: %c\n", i, c)
	}

	bytes := []byte(s)

	for i, c := range bytes {
		fmt.Printf("%d: %c\n", i, c)
	}

	fmt.Println("----")

	for len(bytes) > 0 {
		r, s := utf8.DecodeRune(bytes)
		fmt.Printf("%d: %c\n", s, r)
		bytes = bytes[s:]
	}

	runes := []rune(s)

	fmt.Printf("%q\n", runes)

	//s[1] = 89
}

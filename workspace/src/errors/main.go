package main

import (
	"fmt"
	"log"
)

type myError string

func (me myError) Error() string {
	return string(me)
}

func calculate(klappt bool) (float64, error) {

	if !klappt {
		return 0, myError("hat nicht geklappt")
	}

	return 42, nil
}

func main() {
	// err-lang
	result, err := calculate(false)
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}

	fmt.Println(result)
}

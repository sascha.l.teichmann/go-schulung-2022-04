package greet

import "fmt"

func init() {
	fmt.Println("init 1")
}

func init() {
	fmt.Println("init 2")
}

func Greet() {
	fmt.Println("Hellxxo")
	hidden()
}

func hidden() {
	fmt.Println("versteckt")
}

package main

import "fmt"

type struktur struct {
	A  int `json:"allow"`
	b  string
	c  float64
	fn func() int
	_  [19]byte
}

func doIt(st *struktur) {
	fmt.Println("a:", st.a)
	fmt.Println("fn:", st.fn())
}

func main() {

	var st struktur

	st.a = 43

	st.fn = func() int {
		return st.a * 2
	}

	doIt(&st)

}

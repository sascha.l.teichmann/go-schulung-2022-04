package main

import "fmt"

func test(a, b int) bool {
	return a < b
}

func main() {

	a, b := true, true

	if a && b {
	}

	if cond := test(2, 1); cond {
		fmt.Println("true", cond)
	} else if 10 < 20 {
		fmt.Println("10 < 20")
	} else {
		fmt.Println("false", cond)
	}

	abbruch := false

	for !abbruch { // while
		fmt.Println("xxx")
		abbruch = true
	}

	for { // do ... while
		// ...
		if abbruch {
			break
		}
	}

	x := 10

	switch x := 200; x {
	case 1, 10:
		fmt.Println("1, 10")
	default:
		fmt.Println("sonst")
	}

	switch {
	case x < 10:
		fmt.Println("< 10")
	case x < 100:
		fmt.Println("< 100")
	}
}

package main

import "fmt"

func main() {

	//var m map[string]int
	//m := make(map[string]int)
	m := map[string]int{
		"jürgen": 100,
		"anna":   0,
	}

	m["peter"] = 42

	fmt.Println(m)

	fmt.Println(m["klaus"])
	fmt.Println(m["anna"])

	v, ok := m["klaus"]
	fmt.Println(ok, v)
	v, ok = m["anna"]
	fmt.Println(ok, v)
	fmt.Println("----")

	for k, v := range m {
		fmt.Println(k, v)
	}
}

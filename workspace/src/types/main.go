package main

import "fmt"

type Tree struct {
	left  *Tree
	right *Tree
	value int
}

type Auto int

func (a Auto) brumm() {
	fmt.Printf("brumm: %d\n", a)
}

func (a *Auto) delle() {
	*a -= 10
}

func (t *Tree) inorder(visit func(int)) {
	if t == nil {
		return
	}
	t.left.inorder(visit)
	visit(t.value)
	t.right.inorder(visit)
}

func main() {

	var auto Auto

	auto = 688

	auto *= 2

	fmt.Println(auto)

	auto.brumm()
	auto.delle()
	//(&auto).delle()
	auto.brumm()

	tree := &Tree{
		value: 999,
		left: &Tree{
			value: 888,
		},
		right: &Tree{
			value: 1111,
		},
	}

	tree.inorder(func(v int) {
		fmt.Println("visit:", v)
	})
}

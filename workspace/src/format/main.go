package main

import (
	"fmt"
	"log"
	"math"
	"strconv"
	"strings"
	"text/template"
)

type A int

const tmplTxt = `
Hallo {{ . }}
Das ist ein Musterbrief
`

var txtTmpl = template.Must(template.New("brief").Parse(tmplTxt))

func main() {
	var a A

	fmt.Printf("%q\n", "hello")
	fmt.Printf("%s\n", "hello")
	fmt.Printf("%d\n", 42)
	fmt.Printf("%v\n", 42)
	fmt.Printf("%*d\n", 10, 42)
	fmt.Printf("%.2f\n", math.Pi)
	fmt.Printf("%T\n", a)
	fmt.Printf("%x\n", 2389)

	//strconv.Itoa(348)
	fmt.Println(strconv.FormatInt(2389, 16))

	var b strings.Builder

	if err := txtTmpl.Execute(&b, "Klaus"); err != nil {
		log.Fatalf("error: %v\n", err)
	}

	fmt.Fprintf(&b, "Ende des Musterbriefs\n")

	fmt.Print(b.String())
}

package main

import (
	"fmt"

	"golang.org/x/exp/constraints"
)

/*
type integer interface {
	~int | ~int16 | ~int32
}

type ordered interface {
	integer | ~float64 | ~string
}

*/

func createMap[K comparable, V any](k K, v V) map[K]V {
	return map[K]V{
		k: v,
	}
}

func min[T constraints.Ordered](a, b T) T {
	if a < b {
		return a
	}
	return b
}

type MeinInt int

type tree[V any] struct {
	left  *tree[V]
	right *tree[V]
	value V
}

func (t *tree[V]) inorder(visit func(V)) {
	if t == nil {
		return
	}
	t.left.inorder(visit)
	visit(t.value)
	t.right.inorder(visit)
}

func convert[V, C any](t *tree[V], c C) {
}

/*
func (t *tree[V]) convert[T any](t T) {
}
*/

func returnMe[T any]() T {
	var t T
	return t
}

func main() {

	t := tree[int]{
		value: 10,
	}
	t.inorder(func(v int) {
		fmt.Println("value:", v)
	})

	m := createMap("Klaus", 42)
	fmt.Println(m)

	a, b := MeinInt(43), MeinInt(67)
	m0 := min(a, b)
	fmt.Println(m0)

	m1 := min(1, 3)
	fmt.Println(m1)
	m2 := min(3.14, 1.23)
	fmt.Println(m2)
	m3 := min("Hans", "Aaron")
	fmt.Println(m3)
	var c int = 5
	m4 := min(1.24, float64(c))
	fmt.Println(m4)

}

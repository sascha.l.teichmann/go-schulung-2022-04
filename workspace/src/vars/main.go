package main

var (
	global int
)

func main() {

	var (
		x int32 = 3
	)

	// int8, int16, int32, int64
	// uint8, uint16, uint32, uint64

	// int, uint (mind 32bit)

	// float32, float64

	// complex64, complex128

	// bool

	// uintptr

	// string

	// rune int32

	// byte uint8

	var y int64 = int64(x) // type conversion

	_ = y

	var int float64

	int = 3.1415

	_ = int

	// Operatoren

	// ~ ^

	y++ // y = y + 1 / y += 1
	//++y

	// &= &^=

}

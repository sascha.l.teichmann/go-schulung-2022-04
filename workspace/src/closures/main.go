package main

import (
	"fmt"
	"strconv"
)

func hallo(name string) {
	fmt.Println("Hallo,", name)
}

func gruesser(name string) func() {
	var counter int
	return func() {
		hallo(name + " " + strconv.Itoa(counter))
		counter++
	}
}

func brief(gruss func()) {
	fmt.Println("Sehr geehrte(r)")
	gruss()
	gruss()
}

func main() {

	for _, name := range []string{"Peter", "Klaus", "Anna"} {
		//hallo(name)
		gruss := gruesser(name)
		brief(gruss)
	}
}

package main

import "fmt"

func modify(arr [10]int) {
	arr[0] = 45
}

func main() {

	//var arr [10]int

	arr := [...]int{
		0, 4: 23, 9: 42,
	}

	arr[0] = 1
	arr[len(arr)-1] = 42

	for _, v := range arr {
		fmt.Printf("%d\n", v)
	}

	for i := 0; i < len(arr); i++ {
		fmt.Printf("> %d\n", arr[i])
	}

	modify(arr)

	fmt.Printf("%v\n", arr)

	//x := 12
	//arr[x] = 66
}

package main

import "fmt"

func fixed(s string, a ...int) {
	for _, x := range a {
		fmt.Println(s, x)
	}
}

func main() {
	fmt.Println(1, 2, 3, 4)
	fixed("hello", 1, 2, 3, 4, 5)

	slice := []int{1, 3, 4, 567, 11}
	fixed("world", slice...)

	//fmt.Println("Hello!", "World")
}

package main

import "fmt"

type Auto interface {
	brumm()
	quietsch()
}

func fahre(a Auto) {
	a.brumm()
	a.quietsch()
}

type Wagen int

func (w Wagen) brumm() {
	fmt.Println("wagen: brumm")
}

func (w Wagen) quietsch() {
	fmt.Println("wagen: quietsch")
}

func main() {

	var w Wagen
	fahre(w)

	//fahre(nil)
}

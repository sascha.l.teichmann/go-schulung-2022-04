package main

import "fmt"

func outer() {

	inner := func() {
		fmt.Println("inner")
	}

	inner()
}

func multi(a, b int) (x, y int) {
	//return a * 2, b * 3
	x, y = a*2, b*3
	return
}

func einfach(zahl int, vorname, nachname string) int {
	fmt.Println(zahl)
	zahl++
	fmt.Println(zahl)

	return zahl * 2
}

func main() {
	//var x int = 42
	//x := 42
	var x, y int
	x = 42
	x, y = y, x
	z := einfach(x, "Peter", "Müller")
	fmt.Println(x)
	fmt.Println(z)

	x, _ = multi(4, 5)
	fmt.Printf("m: %d n: %d\n", x, 666)

	var foo, bar, baz bool

	_, _, _ = foo, bar, baz

	outer()
}
